<?php

// for debugging set 1,
// it is bad practice often to poll the server
define('FROM_FILE', 0);
require_once('vendor/autoload.php');

//zip
//https://www.walmart.com/terra-firma/item/5EV2N918N198/location/10001?selected=true

$url = 'https://www.walmart.com/ip/Danskin-Now-Women-s-Knit-Slip-on-Shoe/51630300';
$location = 10001;

if (!is_dir(__DIR__ . '/data/')) {
    mkdir(__DIR__ . '/data/', 0777);
}

// fetch type1
function fetch($url)
{
    preg_match('/\d{1,}$/', $url, $match);
    $file = __DIR__ . '/data/' . $match[0] . '.json';
    if (!FROM_FILE || !file_exists($file)) {
        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 30
        ]);
        $body = curl_exec($ch);
//        $info = curl_getinfo($ch);
//        echo print_r($info, true), PHP_EOL;
        curl_close($ch);
        save($url, $body);
        return $body;
    }
    return file_get_contents($file);
}

// fetch type2
function fetch2($url)
{
    $ch = curl_init($url);
    curl_setopt_array($ch, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_TIMEOUT => 30
    ]);
    $body = curl_exec($ch);
//    $info = curl_getinfo($ch);
//    echo print_r($info, true), PHP_EOL;
    curl_close($ch);
    return $body;
}

// parse
function parse($data)
{
    $pq = phpQuery::newDocument($data);
    foreach ($pq->find('script') as $script) {
        if (pq($script)->attr('src')) {
            continue;
        }
        $data = pq($script)->text();
        if (!preg_match('/^var _setReduxState/', $data, $match)) {
            continue;
        }
        $data = preg_replace('/^var _setReduxState = function\(\) {window\.__WML_REDUX_INITIAL_STATE__ = /', '', $data);
        $data = preg_replace('/;};$/', '', $data);
        break;
    }
    return $data;
}

// save
function save($url, $data)
{
    preg_match('/\d{1,}$/', $url, $match);
    file_put_contents(__DIR__ . '/data/' . $match[0] . '.json', $data);
}

// business logic :)
$data = fetch($url);
$data = parse($data);

$products = [];
$data = json_decode($data, 1);
$products = $data['product']['products'];
$offers = $data['product']['offers'];
$variantCategoriesMap = $data['product']['variantCategoriesMap'];

$primaryId = $data['product']['primaryProduct'];
$data = fetch2('https://www.walmart.com/terra-firma/item/'.$primaryId.'/location/'.$location);
$data = json_decode($data, 1);
$payload = $data['payload'];

$result = [];
$offerId = false;
foreach ($products as $product) {
//    dd($product);
    if ($primaryId == $product['productId']) {
        $offerId = $product['offers'][0];
    }
    $item['productId'] = $product['productId'];
    $item['usItemId'] = $product['usItemId'];
    $item['url'] = preg_replace('/\d{1,}$/', $product['usItemId'], $url);
    $item['offers'] = $offers[$product['offers'][0]]['pricesInfo']['priceMap'];
    $item['status'] = $offers[$product['offers'][0]]['productAvailability']['availabilityStatus'];
    $item['size'] = $variantCategoriesMap[$product['primaryProductId']]['size']['variants'][$product['variants']['size']]['name'];
    $item['color'] = $variantCategoriesMap[$product['primaryProductId']]['actual_color']['variants'][$product['variants']['actual_color']]['name'];
    $result[] = $item;
}

// append last element info about pickup in location 10001
$result[] = [
    'pickup' => $payload['offers'][$offerId]['fulfillment']['pickupOptions']
];

dd($result);